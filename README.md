# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
React versionwas newest v18 but I have downgraded it to v17 so I can easier use Materials UI since I faced
a little complications in order to set MUI on React version v18
In .env I have added REACT_APP_URL variable to have value of backend for this application, so I can use process.env.REACT_APP_URL for bace URL for hiting backend endpoints.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.
Also there are few other scripts created by running cmd npx create-react-app my-app

## About Idea

The theme of this project is to create small contact application, and I developed several functionalities splited into these parts:

User parts
Composed of:

Register
Login
Me (Get me endpoint to fetch Customer's info from bearer token)
Forgot password

Contacts part:
Composed of:

List of all user's contacts
Add new contact
Manage contact: 1. Edit contact, 2. Remove contact

## About Implementation

I have used Typescript in this project.
Whole design is made up of Materials UI components.
I used `redux-store` to create glibal store for storing data received from backend.
To fetch data from backend I have used `redux-saga`.
I have made folder named `redux` which consists of folders `actions`, `reducers`, and `sagas`.
The sagas folder is split into two parts:

1.  `handlers` to create generator functions which will be used to await response from Backend, and then to set data from backend into global store.
2.  `requests` to create axios HTTP requests to request data from backend
