import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";

import Navigation from "./components/Navigation";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Register from "./pages/Register";
import Forgot from "./pages/Forgot";
import Error from "./pages/Error";
import AddContact from "./pages/AddContact";
import ViewContact from "./pages/ViewContact";

function App() {
  return (
    <BrowserRouter>
      <div>
        <Navigation />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/login" element={<Login />} />
          <Route path="/register" element={<Register />} />
          <Route path="/forgot" element={<Forgot />} />
          <Route path="/add" element={<AddContact />} />
          <Route path="/view" element={<ViewContact />} />
          <Route element={<Error />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
