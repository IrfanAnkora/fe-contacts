import * as React from "react";
import { Alert, AlertTitle, Stack } from "@mui/material";

const Error = () => {
  return (
    <Stack sx={{ width: "100%" }} spacing={2}>
      <Alert severity="error">
        <AlertTitle>Error</AlertTitle>
        Ooups!!! Somethint went wrong — <strong>Please come back later!</strong>
      </Alert>
    </Stack>
  );
};

export default Error;
