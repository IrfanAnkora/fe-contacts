import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useLocation } from "react-router-dom";
import {
  Avatar,
  Button,
  CssBaseline,
  TextField,
  Paper,
  Box,
  Grid,
  Typography,
} from "@mui/material";
import { ContactPage } from "@mui/icons-material";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import * as Yup from "yup";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";

import {
  getOneContact,
  getDeleteContact,
  setUpdateContact,
} from "../redux/actions/contact";
import { RootState } from "../redux/reducers";
import { Copyright } from "./Login";

interface Location {
  contactId?: number;
}
const theme = createTheme();

const ViewContact = () => {
  const dispatch = useDispatch();
  const location = useLocation();

  const { contactId } = (location.state as Location) || 0;
  const { contact } = useSelector((state: RootState) => state.contact);

  const [addNewContactData, setAddNewContactData] = useState({
    firstName: "",
    middleName: "",
    lastName: "",
    email: "",
    phone: "",
  });

  const validationSchema = Yup.object().shape({
    firstName: Yup.string().required("First name is required"),
    middleName: Yup.string(),
    lastName: Yup.string().required("Last name is required"),
    email: Yup.string().required("Email is required").email("Email is invalid"),
    phone: Yup.string()
      .required("Phone is required")
      .min(6, "Phone must be at least 6 characters")
      .max(40, "Phone must not exceed 40 characters"),
  });
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(validationSchema),
  });

  const onSubmit = (data: any) => {
    const setContactId = contactId ? contactId : 0;
    dispatch(setUpdateContact(data, setContactId));
  };

  useEffect(() => {
    const setContactId = contactId ? contactId : 0;
    dispatch(getOneContact(setContactId));
    if (contact) {
      setAddNewContactData({
        firstName: contact.firstName,
        middleName: contact.middleName,
        lastName: contact.lastName,
        email: contact.email,
        phone: contact.phone,
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [addNewContactData.firstName]);

  const handleDeleteOneContact = () => {
    const setContactId = contactId ? contactId : 0;
    dispatch(getDeleteContact(setContactId));
  };

  return (
    <ThemeProvider theme={theme}>
      <Grid container component="main" sx={{ height: "100vh" }}>
        <CssBaseline />
        <Grid
          item
          xs={false}
          sm={4}
          md={7}
          sx={{
            backgroundImage: "url(https://source.unsplash.com/random)",
            backgroundRepeat: "no-repeat",
            backgroundColor: (t) =>
              t.palette.mode === "light"
                ? t.palette.grey[50]
                : t.palette.grey[900],
            backgroundSize: "cover",
            backgroundPosition: "center",
          }}
        />
        <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
          <Box
            sx={{
              my: 8,
              mx: 4,
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
              <ContactPage />
            </Avatar>
            <Typography component="h1" variant="h5">
              Manage contact
            </Typography>
            {contact && (
              <Grid container spacing={2}>
                <Grid item xs={12} sm={6}>
                  <TextField
                    autoComplete="given-name"
                    required
                    fullWidth
                    id="firstName"
                    label="First Name"
                    autoFocus
                    defaultValue={contact.firstName}
                    {...register("firstName")}
                    error={errors.firstName ? true : false}
                  />
                  <Typography variant="inherit" color="textSecondary">
                    {errors.firstName?.message}
                  </Typography>
                </Grid>
                <Grid item xs={12} sm={6}>
                  <TextField
                    required
                    fullWidth
                    id="middleName"
                    label="Middle Name"
                    defaultValue={contact.middleName}
                    {...register("middleName")}
                    error={errors.middleName ? true : false}
                  />
                  <Typography variant="inherit" color="textSecondary">
                    {errors.middleName?.message}
                  </Typography>
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    required
                    fullWidth
                    id="lastName"
                    label="Last Name"
                    autoComplete="family-name"
                    defaultValue={contact.lastName}
                    {...register("lastName")}
                    error={errors.lastName ? true : false}
                  />
                  <Typography variant="inherit" color="textSecondary">
                    {errors.lastName?.message}
                  </Typography>
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    required
                    fullWidth
                    id="email"
                    label="Email Address"
                    autoComplete="email"
                    defaultValue={contact.email}
                    {...register("email")}
                    error={errors.email ? true : false}
                  />
                  <Typography variant="inherit" color="textSecondary">
                    {errors.email?.message}
                  </Typography>
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    required
                    fullWidth
                    label="Phone"
                    type="phone"
                    id="phone"
                    autoComplete="new-phone"
                    defaultValue={contact.phone}
                    {...register("phone")}
                    error={errors.phone ? true : false}
                  />
                  <Typography variant="inherit" color="textSecondary">
                    {errors.phone?.message}
                  </Typography>
                </Grid>
              </Grid>
            )}
            <Button
              fullWidth
              variant="contained"
              color="primary"
              sx={{ mt: 3, mb: 2 }}
              onClick={handleSubmit(onSubmit)}
            >
              Save
            </Button>
            <Button
              type="button"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
              onClick={handleDeleteOneContact}
            >
              Delete
            </Button>
          </Box>
          <Copyright sx={{ mt: 5 }} />
        </Grid>
      </Grid>
    </ThemeProvider>
  );
};

export default ViewContact;
