import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import {
  AppBar,
  Button,
  Card,
  CardActions,
  CardContent,
  CssBaseline,
  Grid,
  Stack,
  Box,
  Toolbar,
  Typography,
  Container,
} from "@mui/material";
import { ContactPage, AccountBox } from "@mui/icons-material";
import { createTheme, ThemeProvider } from "@mui/material/styles";

import { Copyright } from "./Login";
import Footer from "../components/Footer";
import { getManyContacts } from "../redux/actions/contact";
import { RootState } from "../redux/reducers";

const theme = createTheme();

const Home = () => {
  const navigate = useNavigate();
  const contacts = useSelector((state: RootState) => state.contact.contacts);

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getManyContacts());
  }, [dispatch]);

  const handleManageContact = (
    event: React.MouseEvent<HTMLElement>,
    contactId: number
  ) => {
    console.log("HIT MANAGE", event.currentTarget, contactId);
    navigate("/view", { state: { contactId } });
  };

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <AppBar position="relative">
        <Toolbar>
          <ContactPage sx={{ mr: 2 }} />
          <Typography variant="h6" color="inherit" noWrap>
            Contacts
          </Typography>
        </Toolbar>
      </AppBar>
      <main>
        {/* Hero unit */}
        <Box
          sx={{
            bgcolor: "background.paper",
            pt: 8,
            pb: 6,
          }}
        >
          <Container maxWidth="sm">
            <Typography
              component="h1"
              variant="h2"
              align="center"
              color="text.primary"
              gutterBottom
            >
              Contacts
            </Typography>
            <Typography
              variant="h5"
              align="center"
              color="text.secondary"
              paragraph
            >
              Hello. Down bellow is your collection of your personal contacts.
              <br />
              You can view, edit or delete them.
            </Typography>
            <Stack
              sx={{ pt: 4 }}
              direction="row"
              spacing={2}
              justifyContent="center"
            />
          </Container>
        </Box>
        <Container sx={{ py: 8 }} maxWidth="md">
          {/* End hero unit */}
          <Grid container spacing={4}>
            {contacts.map((contact: any) => (
              <Grid item key={contact.id} xs={12} sm={6} md={4}>
                <Card
                  sx={{
                    height: "100%",
                    display: "flex",
                    flexDirection: "column",
                  }}
                >
                  <AccountBox style={{ fontSize: 150 }} />
                  <CardContent sx={{ flexGrow: 1 }}>
                    <Typography gutterBottom variant="h5" component="h2">
                      NAME: {contact.firstName} {contact.middleName}
                    </Typography>
                    <Typography>LASTNAME: {contact.lastName}</Typography>
                    <Typography>EMAIL: {contact.email}</Typography>
                    <Typography>PHONE: {contact.phone}</Typography>
                  </CardContent>
                  <CardActions>
                    <Button
                      size="small"
                      onClick={(event) =>
                        handleManageContact(event, contact.id)
                      }
                    >
                      Manage
                    </Button>
                  </CardActions>
                </Card>
              </Grid>
            ))}
          </Grid>
        </Container>
      </main>
      {/* Footer */}
      <Container sx={{ py: 8 }} maxWidth="md">
        <Box sx={{ bgcolor: "background.paper", p: 6 }} component="footer">
          <Footer />
          <Copyright />
        </Box>
      </Container>

      {/* End footer */}
    </ThemeProvider>
  );
};

export default Home;
