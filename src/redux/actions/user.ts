export const GET_USER = "GET_USER";
export const SET_USER = "SET_USER";
export const GET_BEARER = "GET_BEARER";
export const SET_BEARER = "SET_BEARER";
export const GET_REGISTER = "GET_REGISTER";
export const SET_REGISTER = "SET_REGISTER";

export interface LoginData {
  username: string;
  password: string;
}

export interface RegisterData {
  firstName: string;
  middleName: string;
  lastName: string;
  email: string;
  password: string;
}

export interface User {}

export const getUser = () => ({
  type: GET_USER,
});

export const setUser = (user: any) => ({
  type: SET_USER,
  user,
});

export const getBearerToken = (data: LoginData) => ({
  type: GET_BEARER,
  data,
});

export const setBearerToken = (isLogged: boolean) => ({
  type: SET_BEARER,
  isLogged,
});

export const getRegisterUser = (data: RegisterData) => ({
  type: GET_REGISTER,
  data,
});

export const setRegisterUser = (user: User) => ({
  type: SET_REGISTER,
  user,
});
