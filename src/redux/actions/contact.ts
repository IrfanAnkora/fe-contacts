export const GET_CONTACTS = "GET_CONTACTS";
export const SET_CONTACTS = "SET_CONTACTS";
export const ADD_CONTACT = "ADD_CONTACT";
export const SET_CONTACT = "SET_CONTACT";
export const GET_CONTACT = "GET_CONTACT";
export const GET_DELETE_CONTACT = "GET_DELETE_CONTACT";
export const SET_DELETE_CONTACT = "SET_DELETE_CONTACT";
export const SET_UPDATE_CONTACT = "SET_UPDATE_CONTACT";

export interface Contact {
  firstName: string;
  middleName: string;
  lastName: string;
  email: string;
  phone: string;
}

export const getManyContacts = () => ({
  type: GET_CONTACTS,
});

export const setManyContacts = (contacts: Contact) => ({
  type: SET_CONTACTS,
  contacts,
});

export const getCreateContact = (data: Contact) => ({
  type: ADD_CONTACT,
  data,
});

export const setOneContact = (contact: Contact) => ({
  type: SET_CONTACT,
  contact,
});

export const getOneContact = (contactId: number) => ({
  type: GET_CONTACT,
  contactId,
});

export const getDeleteContact = (contactId: number) => ({
  type: GET_DELETE_CONTACT,
  contactId,
});

export const setDeleteContact = () => ({
  type: SET_DELETE_CONTACT,
});

export const setUpdateContact = (data: Contact, contactId: number) => ({
  type: SET_UPDATE_CONTACT,
  data,
  contactId,
});
