import { call, put } from "redux-saga/effects";
import {
  setManyContacts,
  setOneContact,
  setDeleteContact,
} from "../../actions/contact";
import {
  requestGetManyContacts,
  requestCreateContact,
  requestGetOneContact,
  requestDeleteContact,
  requestUpdateContact,
} from "../requests/contact";
import { ResponseGenerator } from "./user";

export function* handleGetManyContacts(action: any) {
  try {
    // 1. make request
    const response: ResponseGenerator = yield call(requestGetManyContacts);
    // 2. extract data from the request
    const { data } = response;
    // 3. store data inside store
    yield put(setManyContacts(data));
  } catch (error) {
    console.log(
      `ERROR: User handler in handleGetManyContacts method: ${error}`
    );
  }
}

export function* handleCreateContact(action: any) {
  try {
    // 1. make request
    const response: ResponseGenerator = yield call(
      requestCreateContact,
      action.data
    );
    // 2. extract data from the request
    const { data } = response;
    // 3. store data inside store
    yield put(setOneContact(data));
  } catch (error) {
    console.log(`ERROR: User handler in handleCreateContact method: ${error}`);
  }
}

export function* handleGetOneContact(action: any) {
  try {
    // 1. make request
    const response: ResponseGenerator = yield call(
      requestGetOneContact,
      action.contactId
    );
    //2. extract data from the request
    const { data } = response;
    // 3. store data inside store
    yield put(setOneContact(data));
  } catch (error) {
    console.log(`ERROR: User handler in handleGetOneContact method: ${error}`);
  }
}

export function* handleDeleteContact(action: any) {
  try {
    // 1. make request
    yield call(requestDeleteContact, action.contactId);
    // 2. store data inside store
    yield put(setDeleteContact());
  } catch (error) {
    console.log(`ERROR: User handler in handleDeleteContact method: ${error}`);
  }
}

export function* handleUpdateContact(action: any) {
  try {
    // 1. make request
    const response: ResponseGenerator = yield call(
      requestUpdateContact,
      action.data,
      action.contactId
    );
    // 2. store data inside store
    const { data } = response;
    console.log("the data", data);
    // 3. store data inside store
    yield put(setOneContact(data));
  } catch (error) {
    console.log(`ERROR: User handler in handleUpdateContact method: ${error}`);
  }
}
