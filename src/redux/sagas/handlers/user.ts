import { call, put } from "redux-saga/effects";
import { setUser, setBearerToken } from "../../actions/user";
import {
  requestGetBearerToken,
  // requestGetUser,
  requestRegisterUser,
} from "../requests/user";

export interface ResponseGenerator {
  config?: any;
  data?: any;
  headers?: any;
  request?: any;
  status?: number;
  statusText?: string;
}

// export function* handleGetUser(action: any) {
//   try {
//     // 1. make request
//     const response: ResponseGenerator = yield call(requestGetUser);
//     // 2. extract data from the request
//     const { data } = response;
//     // 3. store data inside of our actual reducer
//     yield put(setUser(data));
//   } catch (error) {
//     console.log(`ERROR: User handler in handleGetUser method: ${error}`);
//   }
// }

export function* handleGetBearerToken(action: any) {
  try {
    // 1. make request
    const response: ResponseGenerator = yield call(
      requestGetBearerToken,
      action.data
    );
    // 2. extract data from the request
    const { data } = response;
    if (data.access_token) {
      // 3. store data inside local storage
      localStorage.setItem("CONTACTS-TOKEN", data.access_token);
      // 4. store data inside of our actual reducer
      yield put(setBearerToken(true));
    }
  } catch (error) {
    console.log(`ERROR: User handler in handleGetBearerToken method: ${error}`);
  }
}

export function* handleRegisterUser(action: any) {
  try {
    // 1. make request
    const response: ResponseGenerator = yield call(
      requestRegisterUser,
      action.data
    );
    // 2. extract data from the request
    const { data } = response;
    // 3. store data inside of our actual reducer
    yield put(setUser(data));
  } catch (error) {
    console.log(`ERROR: User handler in handleRegisterUser method: ${error}`);
  }
}
