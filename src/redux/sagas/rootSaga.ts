import { takeLatest } from "redux-saga/effects";
import { GET_BEARER, GET_REGISTER } from "../actions/user";
import {
  GET_CONTACTS,
  ADD_CONTACT,
  GET_CONTACT,
  GET_DELETE_CONTACT,
  SET_UPDATE_CONTACT,
} from "../actions/contact";
import { handleGetBearerToken, handleRegisterUser } from "./handlers/user";
import {
  handleGetManyContacts,
  handleCreateContact,
  handleGetOneContact,
  handleDeleteContact,
  handleUpdateContact,
} from "./handlers/contact";

export function* watcherSaga() {
  yield takeLatest(GET_BEARER, handleGetBearerToken);
  yield takeLatest(GET_REGISTER, handleRegisterUser);
  yield takeLatest(GET_CONTACTS, handleGetManyContacts);
  yield takeLatest(ADD_CONTACT, handleCreateContact);
  yield takeLatest(GET_CONTACT, handleGetOneContact);
  yield takeLatest(GET_DELETE_CONTACT, handleDeleteContact);
  yield takeLatest(SET_UPDATE_CONTACT, handleUpdateContact);
}
