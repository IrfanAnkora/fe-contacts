import axios from "axios";
import { LoginData, RegisterData } from "../../actions/user";
const BASE_URL = process.env.REACT_APP_URL;

export const requestGetBearerToken = (data: LoginData) => {
  return axios.request({
    method: "POST",
    url: `${BASE_URL}/user/login`,
    data,
  });
};

export const requestRegisterUser = (data: RegisterData) => {
  return axios.request({
    method: "POST",
    url: `${BASE_URL}/user`,
    data,
  });
};
