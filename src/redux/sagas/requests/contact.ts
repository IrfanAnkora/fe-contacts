import axios from "axios";
import { Contact } from "../../actions/contact";
const BASE_URL = process.env.REACT_APP_URL;

const getBearerToken = (): string | null => {
  return localStorage.getItem("CONTACTS-TOKEN");
};

export const requestGetManyContacts = () => {
  return axios.request({
    method: "GET",
    url: `${BASE_URL}/contacts`,
    headers: {
      Authorization: `Bearer ${getBearerToken()}`,
    },
  });
};

export const requestCreateContact = (data: Contact) => {
  return axios.request({
    method: "POST",
    url: `${BASE_URL}/contacts`,
    data,
    headers: {
      Authorization: `Bearer ${getBearerToken()}`,
    },
  });
};

export const requestGetOneContact = (contactId: number) => {
  return axios.request({
    method: "GET",
    url: `${BASE_URL}/contacts/${contactId}`,
    headers: {
      Authorization: `Bearer ${getBearerToken()}`,
    },
  });
};

export const requestDeleteContact = (contactId: number) => {
  return axios.request({
    method: "DELETE",
    url: `${BASE_URL}/contacts/${contactId}`,
    headers: {
      Authorization: `Bearer ${getBearerToken()}`,
    },
  });
};

export const requestUpdateContact = (data: Contact, contactId: number) => {
  return axios.request({
    method: "PATCH",
    url: `${BASE_URL}/contacts/${contactId}`,
    data,
    headers: {
      Authorization: `Bearer ${getBearerToken()}`,
    },
  });
};
