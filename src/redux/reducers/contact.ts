import {
  SET_CONTACTS,
  SET_CONTACT,
  SET_DELETE_CONTACT,
} from "../actions/contact";
import { STATUS_ENUM } from "./user";

const initialState = {
  status: STATUS_ENUM.PENDING,
  contacts: [],
  contact: undefined,
};

const contactReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case SET_CONTACTS:
      const { contacts } = action;
      return { ...state, contacts, status: STATUS_ENUM.SUCCESS };
    case SET_CONTACT:
      const { contact } = action;
      return { ...state, contact, status: STATUS_ENUM.SUCCESS };
    case SET_DELETE_CONTACT:
      return { ...state, contact: undefined, status: STATUS_ENUM.SUCCESS };
    default:
      return state;
  }
};

export default contactReducer;
