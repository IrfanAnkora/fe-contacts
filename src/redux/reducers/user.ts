import { SET_BEARER, SET_USER } from "../actions/user";

export enum STATUS_ENUM {
  SUCCESS = "SUCCESS",
  FAILURE = "FAILURE",
  PENDING = "PENDING",
}
const initialState = {
  status: STATUS_ENUM.PENDING,
  user: undefined,
  isLogged: false,
};

const userReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case SET_USER:
      const { user } = action;
      return { ...state, user, status: STATUS_ENUM.SUCCESS };
    case SET_BEARER:
      const { isLogged } = action;
      return { ...state, isLogged, status: STATUS_ENUM.SUCCESS };
    default:
      return state;
  }
};

export default userReducer;
