import { combineReducers } from "redux";
import { configureStore } from "@reduxjs/toolkit";
import createSagaMiddleware from "@redux-saga/core";

import { watcherSaga } from "../sagas/rootSaga";
import userReducer from "./user";
import contactReducer from "./contact";

export const rootReducers = combineReducers({
  user: userReducer,
  contact: contactReducer,
});

const sagaMiddleware = createSagaMiddleware();

const middleware = [sagaMiddleware];

const store = configureStore({
  reducer: rootReducers,
  middleware: middleware,
  devTools: true,
});

sagaMiddleware.run(watcherSaga);

export type RootState = ReturnType<typeof rootReducers>;

export default store;
