import * as React from "react";
import { Box, BottomNavigation, BottomNavigationAction } from "@mui/material";
import { Restore, Favorite, LocationOn } from "@mui/icons-material";

const Footer = () => {
  const [value, setValue] = React.useState(0);

  return (
    <Box
      sx={{
        bgcolor: "background.paper",
        pb: 3,
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
      }}
    >
      <BottomNavigation
        showLabels
        value={value}
        onChange={(event, newValue) => {
          setValue(newValue);
        }}
      >
        <BottomNavigationAction label="Recents" icon={<Restore />} />
        <BottomNavigationAction label="Favorites" icon={<Favorite />} />
        <BottomNavigationAction label="Nearby" icon={<LocationOn />} />
      </BottomNavigation>
    </Box>
  );
};
export default Footer;
